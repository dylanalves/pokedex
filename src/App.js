import React, { Component } from 'react';
import './App.css';
import Navbar from './components/navbar'
import Pokemons from './components/pokemons'
import Footer from './components/footer'

class App extends Component {
  render() {
    return (
      <div className="App">
          <Navbar />
          <Pokemons />
          <Footer />
      </div>
    );
  }
}

export default App;
