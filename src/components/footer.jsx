import React from 'react'

const footer = props =>{
    return(
        <footer className='footer'>
            <div className='container'>
                <div className='columns'>

                    <div className="column has-text-centered">
                        <p>
                        <strong>React Pokedex</strong> by <a target="_blank" href="https://www.facebook.com/TheDylanAlves">Dylan Alves</a>.  
                        </p>
                    </div>
                </div>
                <div className='columns'>
                    <div className='column has-text-centered'>
                        <p className='is-size-4 has-text-centered'>Stack</p>
                    </div>
                </div>
                <div className='columns'>
                    <div className='column is-4'>
                        <ul>
                            <li><a className='grey-text text-lighten-3'target="_blank" href='https://pokeapi.co/'>PokeAPI</a></li>
                            <li><a className='grey-text text-lighten-3'target="_blank" href='https://play.pokemonshowdown.com/sprites/xyani/'>Pokemon Showdown</a></li>
                        </ul>   
                    </div>
                    <div className='column is-4'>
                        <ul>
                            <li><a className='grey-text text-lighten-3' target="_blank" href='http://jerairrest.github.io/react-chartjs-2/'>React ChartJS 2</a></li>
                            <li><a className='grey-text text-lighten-3' target="_blank" href='https://www.npmjs.com/package/react-spinners'>React Spinner</a></li>
                        </ul>   
                    </div>
                    <div className='column is-4'>
                        <ul>
                            <li><a className='grey-text text-lighten-3' target="_blank" href='https://reactjs.org/'>React JS</a></li>
                            <li><a className='grey-text text-lighten-3' target="_blank" href='https://bulma.io/'>Bulma</a></li>
                        </ul>  
                    </div>
                </div>
                <div className='columns'>
                    <div className='column has-text-centered has-text-white'>
                        <p>All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default footer