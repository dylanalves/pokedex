import React from 'react'
import logo from '../img/pokedex.svg'

const Navbar = props => {
    return(
        <div>
              <nav className='navbar is-light'>
                <div className='navbar-item'>
                    <figure>
                        <a href='/'>
                            <img src={logo} alt='logo'/>
                        </a>
                    </figure>
                </div>
            </nav>
        </div>
        
    )
}

export default Navbar