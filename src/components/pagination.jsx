import React, { Component } from 'react'

class Pagination extends Component {

    constructor(props){
        super(props);
    }

    render(){
        return (
            <nav className='pagination is-medium' role='navigation' aria-label='pagination'>
                <ul className='pagination-list'>
                    <li className=''>
                        <a className='pagination-link' onClick={this.props.firstPage}
                                                        disabled={!this.props.previous}>
                            <span className='has-text-link icon is-small' >
                                <i className='fas fa-angle-double-left'></i>
                            </span>
                        </a>
                    </li>
                    <li className=''>
                        <a className='pagination-link'  onClick={this.props.previousPage}
                                                        disabled={!this.props.previous}>
                            <span className='has-text-link icon is-small'>
                                <i className='fas fa-angle-left'></i>
                            </span>
                        </a>
                    </li>
                    <li className=''>
                        <a className='pagination-link'  onClick={this.props.nextPage}
                                                        disabled={!this.props.next}>
                            <span className='has-text-link icon is-small'>
                                <i className='fas fa-angle-right'></i>
                            </span>
                        </a>
                    </li>
                    <li className=''>
                        <a className='pagination-link'  onClick={this.props.lastPage}
                                                        disabled={!this.props.next}>
                            <span className='has-text-link icon is-small'>
                                <i className='fas fa-angle-double-right'></i>
                            </span>
                        </a>
                    </li>  
                </ul>
            </nav>
        )
    }
}

export default Pagination