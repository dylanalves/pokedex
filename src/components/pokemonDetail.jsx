import React, { Component } from 'react'
import {BrowserRouter as Router, Link} from 'react-router-dom'
import axios from 'axios'
import { BounceLoader } from 'react-spinners' 
import StatsChart from './statsChart'
import PokemonEntry from './pokemonEntry';


class PokemonDetail extends Component {
    constructor(props){
        super(props);

        this.state = {
            isActive : false,
            pokemon : '', 
            isLoaded : false,
            types : [],
            stats : [],
            specie : '',
            textEntries : []
        }

        this.goBack = this.goBack.bind(this);
    }
    
    goBack(){
                
        this.props.history.goBack();
    }

    handleToggle(e){
        e.preventDefault();
        this.setState({
            isActive : !this.state.isActive
        })
    }

    componentDidMount(){

        axios.get(this.props.url)
             .then(response => {
                 this.setState({
                     pokemon : response.data, 
                     types : response.data.types, 
                     stats : response.data.stats,
                     specie : response.data.species.url
                    })
                    
                return axios.get(this.state.specie)
             })
             .then(response => {
                 this.setState({
                    textEntries : response.data.flavor_text_entries,
                    isLoaded : true
                 })
             })
             .catch( error => {
                 console.log('Erro ao obter informações do Pokemon: ' + error)
             })
    }


    render(){

        const {name, sprite} = this.props
        const isLoaded = this.state.isLoaded

        const pokemonDetail = <div className='modal-content'>
                    <div className='modal-card'>
                        <header className="modal-card-head">
                            <p className="modal-card-title has-text-left pokemonTitleName">{name}</p>
                        </header>
                        <section className="modal-card-body">
                            <div className='columns'>
                                <div className='column is-vertical-center'>
                                    <figure>
                                        <img src={sprite}/>
                                    </figure>
                                </div>
                                <div className='column'>
                                    <StatsChart name={name}  stats={this.state.pokemon.stats}/>
                                </div>
                            </div>
                            <div className='columns'>
                                    <div className='column'>
                                        <p>
                                            <strong>Height:</strong> {(this.state.pokemon.height / 10)}m 
                                        </p>
                                    </div>
                                    <div className='column'>
                                        <p>
                                            <strong>Weight:</strong> {(this.state.pokemon.weight / 10)}kg 
                                        </p>
                                    </div>
                                </div>
                            <div className='columns'>
                                <div className='column'>
                                    {this.state.textEntries.slice(0,3).map(entry => {
                                        if(entry.language.name === 'en'){
                                            return <PokemonEntry blockquote={entry.flavor_text} version={entry.version.name}/>
                                        }
                                    })}
                                    
                                    <ul>
                                        {this.state.types.map((types, i) => (
                                            <li key={i} className={`tag ${types.type.name} is-medium`} >{types.type.name}</li>  
                                            ))
                                        }
                                    </ul>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>


        return (
            <Router>
               <div className={`modal ${!this.state.isActive ? 'is-active' : '' }`}>
                    <div className='modal-background' onClick={e => {
                                                this.handleToggle(e); 
                                                this.goBack();
                                            }}
                    >
                    </div>
                    
                    {isLoaded ? (
                        pokemonDetail
                    ) : (
                         <BounceLoader className='override center' sizeUnit={'px'} size={60} color={'#3273dc'} loading={true}/>
                    )}
                    
                    <Link   to='/' className='modal-close is-large' 
                            onClick={e => {
                                            this.handleToggle(e); 
                                            this.goBack();
                                          }
                                    }>
                    </Link>
                </div>
            </Router>
        )
    }
}

export default PokemonDetail