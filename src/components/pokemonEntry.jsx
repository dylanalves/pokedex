import React, { Component } from 'react'
import axios from 'axios'
import { BounceLoader } from 'react-spinners' 

class PokemonEntry extends Component{

    constructor(props){
        super(props)
    }


    render(){
        return(
            <div className='content'>
                <blockquote className='has-text-left'>
                    {this.props.blockquote}
                    <p className='is-size-7 is-capitalized'>-{this.props.version}</p>
                </blockquote>
            </div>
        )
    }
}

export default PokemonEntry