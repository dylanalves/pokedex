import React, { Component } from 'react'
import { BrowserRouter as Router, Link, Route} from 'react-router-dom'
import PokemonDetail from './pokemonDetail'
import notFound from '../img/notFound.png'

class PokemonListItem extends Component {
    constructor(props){
        super(props)

        this.state = {
            renderModal : false,
            sprite : `http://play.pokemonshowdown.com/sprites/xyani/${this.props.name}.gif`
        }

        this.handleImg = this.handleImg.bind(this)
    }

    handleImg(){

        this.setState({
            sprite: notFound
        })
    }

    render(){

        const  {name, url} = this.props;
        return (
            <Router>
                <div className='column is-3'>
                    <div className='card '>
                        <div className='card-image cardHeight is-vertical-center'>
                            <figure>
                                <img onError={this.handleImg} className='sprite' alt='Pokemon Image' src={this.state.sprite} />
                            </figure>
                        </div>
                        <div className='card-content'>
                            <div className='media-content'>
                                <p className='title is-6 pokemonTitleName'>{name}</p>
                            </div>
                            <div className='card-footer'>
                                <div className='card-footer-item'>

                                    <Route  path={`/pokemonDetail=${name}`}  render={(props) => <PokemonDetail {...props} name={name} url={url} sprite={this.state.sprite}/>}/> 
                                    <Link   to={`/pokemonDetail=${name}`}
                                            className='button is-fullwidth has-text-link is-outlined'>Details
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Router>
        )
    }
}

export default PokemonListItem