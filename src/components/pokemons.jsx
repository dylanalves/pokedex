import React, { Component } from 'react'
import axios from 'axios'
import PokemonListItem from './pokemonListItem'
import { BounceLoader } from 'react-spinners'
import Pagination from './pagination';
import SearchBar from './searchBar';

class Pokemons extends Component {

    constructor(props){
        super(props);

        this.state = {
            pokemons : [],
            isLoaded : false,
            url : 'https://pokeapi.co/api/v2/pokemon/',
            count : '',
            next : '',
            previous : ''
        }

        this.previousPage = this.previousPage.bind(this)
        this.nextPage = this.nextPage.bind(this)
        this.firstPage = this.firstPage.bind(this)
        this.lastPage = this.lastPage.bind(this)
        this.search = this.search.bind(this)
    }

    previousPage(){
        this.setState({
            url : this.state.previous,
            isLoaded : false
        })
    }

    nextPage(){
        this.setState({
            url : this.state.next,
            isLoaded : false
        })
    }

    firstPage(){
        this.setState({
            url : 'https://pokeapi.co/api/v2/pokemon/',
            isLoaded : false
        })
    }

    lastPage(){
        const offset = (Math.ceil(this.state.count/20) -1) * 20

        this.setState({
            url : 'https://pokeapi.co/api/v2/pokemon/?limit=20&offset=' + offset,
            isLoaded : false
        })
    }

    search(e){
        this.setState({url : 'https://pokeapi.co/api/v2/pokemon/' + e.target.value})
    }

    componentWillMount(){
        
        axios.get(this.state.url)
                .then( response => { 

                    // if(response.results !== undefined){
                        this.setState({
                            pokemons : response.data.results, 
                            count : response.data.count,
                            next : response.data.next,
                            previous : response.data.previous,
                            isLoaded : true,
                        });
                    // }else {
                    //     this.setState({
                    //         pokemons : response.data + this.state, 
                    //         isLoaded : true,
                    //     });
                    // }
                    // console.log(response.data.results)
                })
                .catch( error => {
                    this.setState({
                        isLoaded : false
                    });
                    console.log('Pokedex Offline: ' + error)
                });
    }

    componentDidUpdate(prevProps, prevState){

        if(prevState.url !== this.state.url){
            axios.get(this.state.url)
                .then( response => { 
                    this.setState({
                        pokemons : response.data.results, 
                        isLoaded : true,
                        next : response.data.next,
                        previous : response.data.previous
                    });
                })
                .catch( error => {
                    console.log('Pokedex Offline: ' + error)
                });
        }
    }

    render(){
        const pokemons = <ul className='columns is-multiline'>
                            {
                                 this.state.pokemons.map(pokemons => 
                                                        <PokemonListItem    key={pokemons.name}
                                                                            name={pokemons.name}
                                                                            url={pokemons.url}
                                                                            />)
                            }
                            
                         </ul> 

        const isLoaded = this.state.isLoaded

        return (
            <div className='section'>
                <div className='container'>
                    <div className='columns'>
                        <div className='column is-9'>
                            {/* <SearchBar search={this.search}/> */}
                        </div>
                        <div className='column is-3'>
                            <Pagination previousPage={this.previousPage} 
                                    nextPage={this.nextPage} 
                                    firstPage={this.firstPage}
                                    lastPage={this.lastPage}
                                    previous={this.state.previous}
                                    next={this.state.next}/>
                        </div>
                    </div>
                                     
                    <div className='columns is-centered '>
                        {isLoaded ? (
                            pokemons
                        ) : (
                            <div className='is-vertical-center'>
                                <BounceLoader sizeUnit={'px'} size={120} color={'#3273dc'} loading={true}/>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        )    
    }
}

export default Pokemons