import React, { Component } from 'react'

class SearchBar extends Component {

    constructor(props){
        super(props)

    }

    render(){
        return(
            <div className='field'>
                <p className='control has-icons-left'>
                    <input className='input is-medium' type='text' placeholder='Type a Pokemon here!' onChange={this.props.search.bind(this)}/>
                    <span className='icon is-small is-left'>
                        <i className='fas fa-search'></i>
                    </span>
                </p>
            </div>
        )
    }
}

export default SearchBar