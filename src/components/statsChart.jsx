import React, { Component } from 'react'
import { Radar } from 'react-chartjs-2'

class StatsChart extends Component {

    constructor(props){
        super(props)
    }

    render(){

        const data = {
            labels : ['Speed', 'Spx Defense', 'Spx Attack', 'Defense', 'Attack', 'HP'],
            datasets : [
                {
                    label : 'Status',
                    backgroundColor : 'rgba(50, 115, 220, 0.1)',
                    borderColor : '#3273dc',
                    pointBackgroundColor : '#3273dc',
                    pointBorderColor : '#fff',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#3273dc',
                    data: [
                        this.props.stats[0].base_stat,
                        this.props.stats[1].base_stat,
                        this.props.stats[2].base_stat,
                        this.props.stats[3].base_stat,
                        this.props.stats[4].base_stat,
                        this.props.stats[5].base_stat]
                }
            ]
        }

        //console.log(data)
        return(
                
            <div className='chart'>
               <Radar data={data} height={50} width={100} legend={false}/>
            </div>
        )
    }
}

export default StatsChart